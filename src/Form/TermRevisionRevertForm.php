<?php

namespace Drupal\taxonomy_term_revision\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a confirmation form to confirm reverting to a term revision by id.
 */
class TermRevisionRevertForm extends ConfirmFormBase {

  /**
   * The revision id.
   *
   * @var string
   */
  protected string $id;

  /**
   * The entity id.
   *
   * @var string
   */
  protected string $entityId;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The time details.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * User definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a new TermRevisionRevertForm.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter.
   */
  public function __construct(Connection $database, LoggerChannelFactoryInterface $loggerFactory, TimeInterface $time, EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $current_user, DateFormatterInterface $date_formatter) {
    $this->database = $database;
    $this->loggerFactory = $loggerFactory;
    $this->time = $time;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "term_revision_revert_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $taxonomy_term = NULL, $id = NULL): array {
    $this->id = $id;
    if ($taxonomy_term instanceof TermInterface) {
      $this->entityId = $taxonomy_term->id();
    }
    else {
      $this->entityId = $taxonomy_term;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->loadRevision($this->id);
    if (!empty($term)) {
      $original_revision_timestamp = $term->getRevisionCreationTime();
      if (empty($original_revision_timestamp)) {
        $term->setRevisionLogMessage("Copy of the revision from " . $this->id);
      }
      else {
        $term->setRevisionLogMessage("Copy of the revision from " . $this->dateFormatter->format($original_revision_timestamp));
      }
      $term->setRevisionUserId($this->currentUser->id());
      $term->setRevisionCreationTime($this->time->getRequestTime());
      $term->setChangedTime($this->time->getRequestTime());
      $term->setNewRevision();
      $term->isDefaultRevision(TRUE);
      $term->save();

      $this->loggerFactory->get('taxonomy_term_revision')->info('Term reverted tid %tid revision_id %trid', [
        '%tid' => $this->entityId,
        '%trid' => $this->id,
      ]);
      $this->messenger()->addStatus($this->t('This term has been reverted'));
    }
    else {
      $this->messenger()->addError($this->t('Error! Revision Id does not exist for given Term Id'));
    }

    // Redirect to Revision Page of the term.
    $response = new RedirectResponse(Url::fromRoute('taxonomy_term_revision.all', ['taxonomy_term' => $this->entityId])->toString());
    $response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('taxonomy_term_revision.all', ['taxonomy_term' => $this->entityId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Do you want to revert to this revision?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return $this->t('You can always revert to current revision.');
  }

}
