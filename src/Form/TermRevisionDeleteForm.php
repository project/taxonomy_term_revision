<?php

namespace Drupal\taxonomy_term_revision\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a confirmation form to confirm deletion of term revision by id.
 */
class TermRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The revision id.
   *
   * @var string
   */
  protected string $id;

  /**
   * The entity id.
   *
   * @var string
   */
  protected string $entityId;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * TThe entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new TermRevisionDeleteForm.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(Connection $database, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->loggerFactory = $logger_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "term_revision_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $taxonomy_term = NULL, $id = NULL): array {
    $this->id = $id;
    if ($taxonomy_term instanceof TermInterface) {
      $this->entityId = $taxonomy_term->id();
    }
    else {
      $this->entityId = $taxonomy_term;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entityTypeManager->getStorage('taxonomy_term')->deleteRevision($this->id);
    $result = $this->entityTypeManager->getStorage('taxonomy_term')->loadRevision($this->id);

    if ($result == NULL) {
      $this->loggerFactory->get('taxonomy_term_revision')->info('Term revision deleted tid %tid revision_id %trid', [
        '%tid' => $this->entityId,
        '%trid' => $this->id,
      ]);
      $this->messenger()->addStatus($this->t('Revision has been deleted'));
    }
    else {
      $this->messenger()->addError($this->t('Error! Revision Id does not exist for given Term Id'));
    }
    // Redirect to Revision page of the term.
    $response = new RedirectResponse(Url::fromRoute('taxonomy_term_revision.all', ['taxonomy_term' => $this->entityId])->toString());
    $response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('taxonomy_term_revision.all', ['taxonomy_term' => $this->entityId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Do you want to delete this revision?');
  }

}
