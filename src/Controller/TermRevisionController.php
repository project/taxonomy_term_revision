<?php

namespace Drupal\taxonomy_term_revision\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\GeneratedLink;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The term revision controller.
 */
class TermRevisionController extends ControllerBase {

  /**
   * The table name of taxonomy revisions.
   */
  protected const TAXONOMY_REVISION_TABLE = 'taxonomy_term_revision';

  /**
   * The permission name to revert term revision.
   */
  protected const REVERT_TERM_REVISION_PERMISSION = 'revert term revision';

  /**
   * The permission name to delete term revision.
   */
  protected const DELETE_TERM_REVISION_PERMISSION = 'delete term revision';

  /**
   * The revert operation.
   */
  protected const REVERT = 'Revert';

  /**
   * The delete operation.
   */
  protected const DELETE = 'Delete';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The user storage details connection.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected UserStorageInterface $userStorage;

  /**
   * The entity term manager.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected TermStorageInterface $termManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The term view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected EntityViewBuilderInterface $termViewBuilder;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a new TermRevisionController.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   User storage.
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   Term storage.
   * @param \Drupal\Core\Entity\EntityRepository $entity_repository
   *   Entity repository.
   * @param \Drupal\Core\Entity\EntityViewBuilderInterface $term_view_builder
   *   Term view builder.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter.
   */
  public function __construct(Connection $database, UserStorageInterface $user_storage, TermStorageInterface $term_storage, EntityRepository $entity_repository, EntityViewBuilderInterface $term_view_builder, DateFormatterInterface $date_formatter) {
    $this->database = $database;
    $this->userStorage = $user_storage;
    $this->termManager = $term_storage;
    $this->entityRepository = $entity_repository;
    $this->termViewBuilder = $term_view_builder;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager')->getViewBuilder('taxonomy_term'),
      $container->get('date.formatter')
    );
  }

  /**
   * Getting all revisions.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route.
   *
   * @return array
   *   The array of taxonomy term revisions.
   */
  public function getRevisions(RouteMatchInterface $route_match): array {
    /** @var \Drupal\taxonomy\TermInterface $taxonomy_term */
    $taxonomy_term = $route_match->getParameter('taxonomy_term');
    $schema = $this->database->schema();
    if (!($taxonomy_term instanceof TermInterface) || !$schema->tableExists(self::TAXONOMY_REVISION_TABLE)) {
      return [
        '#markup' => $this->t('No revisions available'),
      ];
    }
    // Query for fetching term revision data.
    $results = $this->fetchTaxonomyRevisionData(intval($taxonomy_term->id()));
    if (empty($results)) {
      return [
        '#markup' => $this->t('No revisions available'),
      ];
    }

    // Header for revision table.
    $header = [
      $this->t('CHANGED'),
      $this->t('USER'),
      $this->t('OPERATIONS'),
      $this->t('LOG MESSAGE'),
    ];

    // Data to be rendered on revisions page of a term.
    $data = [];
    foreach ($results as $index => $result) {
      $revision_user = $this->getRevisionUser($result->revision_user);

      // Check if Current revision.
      $term = $this->termManager->loadRevision($result->revision_id);
      $date = $this->dateFormatter->format($result->revision_created, 'short');
      if ($term->isDefaultRevision()) {
        $data[$index] = [
          Link::fromTextAndUrl($date, Url::fromUri('internal:/taxonomy/term/' . $taxonomy_term->id())),
          $revision_user,
          $this->t('Current Revision'),
          $result->revision_log_message,
        ];
      }
      else {
        $revert_link = $this->currentUser()->hasPermission(self::REVERT_TERM_REVISION_PERMISSION) ? $this->getLink(self::REVERT, $taxonomy_term->id(), $result->revision_id) : '';
        $delete_link = $this->currentUser()->hasPermission(self::DELETE_TERM_REVISION_PERMISSION) ? $this->getLink(self::DELETE, $taxonomy_term->id(), $result->revision_id) : '';

        $data[$index] = [
          Link::fromTextAndUrl($date, Url::fromRoute('taxonomy_term_revision.view', [
            'taxonomy_term' => $taxonomy_term->id(),
            'revision_id' => $result->revision_id,
          ])),
          $revision_user,
          $this->t('@revert | @delete', [
            '@revert' => $revert_link,
            '@delete' => $delete_link,
          ]),
          $result->revision_log_message,
        ];
      }
    }
    return ['#type' => 'table', '#header' => $header, '#rows' => $data];
  }

  /**
   * Fetches revision data of the specified term.
   *
   * @param int $term_id
   *   The term id.
   *
   * @return array
   *   Returns array of taxonomy revisions.
   */
  protected function fetchTaxonomyRevisionData(int $term_id): array {
    return $this->database->select(self::TAXONOMY_REVISION_TABLE, 'tr')
      ->fields('tr', [
        'revision_id',
        'revision_created',
        'revision_default',
        'revision_user',
        'revision_log_message',
      ])
      ->condition('tr.tid', $term_id)
      ->orderBy('revision_id', 'DESC')
      ->execute()
      ->fetchAll();
  }

  /**
   * Returns the link for the specified operation.
   *
   * @param string $operation
   *   Operation to generate the link.
   * @param string $term_id
   *   Term id.
   * @param string $revision_id
   *   Revision id of the term.
   *
   * @return \Drupal\Core\GeneratedLink
   *   Returns the generated link.
   */
  protected function getLink(string $operation, string $term_id, string $revision_id): GeneratedLink {
    if ($operation === self::REVERT) {
      return Link::fromTextAndUrl(self::REVERT, Url::fromRoute('taxonomy_term_revision.revert', [
        'taxonomy_term' => $term_id,
        'id' => $revision_id,
      ]))->toString();
    }
    if ($operation === self::DELETE) {
      return Link::fromTextAndUrl(self::DELETE, Url::fromRoute('taxonomy_term_revision.delete', [
        'taxonomy_term' => $term_id,
        'id' => $revision_id,
      ]))->toString();
    }

    return Link::fromTextAndUrl('View', Url::fromRoute('taxonomy_term_revision.view', [
      'taxonomy_term' => $term_id,
      'id' => $revision_id,
    ]))->toString();
  }

  /**
   * Fetches name of the revision user's name.
   *
   * @param null|string $revision_user
   *   The id of the revision created user.
   *
   * @return \Drupal\Core\Link
   *   Returns link with the revision user's name.
   */
  protected function getRevisionUser(?string $revision_user): Link {
    if (empty($revision_user)) {
      return Link::fromTextAndUrl($this->t('Anonymous'), Url::fromUri('internal:/user/' . $revision_user));
    }
    $user = $this->userStorage->load($revision_user);
    return Link::fromTextAndUrl($user->get('name')->getString(), Url::fromUri('internal:/user/' . $revision_user));
  }

  /**
   * Getting revision data.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The taxonomy term.
   *
   * @return array
   *   The term page.
   */
  public function revisionShow(RouteMatchInterface $route_match): array {
    $revision_id = $route_match->getParameter('revision_id');
    $term = $this->termManager->loadRevision($revision_id);
    $term = $this->entityRepository->getTranslationFromContext($term);
    $page = $this->termViewBuilder->view($term);
    unset($page['#cache']);
    return $page;
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The taxonomy term.
   *
   * @return array
   *   The term label as a render array.
   */
  public function revisionPageTitle(RouteMatchInterface $route_match): array {
    $revision_id = $route_match->getParameter('revision_id');
    $taxonomy_term_revision = $this->termManager->loadRevision($revision_id);
    return [
      '#markup' => $taxonomy_term_revision->getName(),
      '#allowed_tags' => Xss::getHtmlTagList(),
    ];
  }

}
